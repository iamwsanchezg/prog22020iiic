package views;

import java.awt.Button;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import misc.event.FormInterface;

public class MainFrame extends Frame implements FormInterface{
	/**
	 * author: William Sanchez
	 * Description: Formulario principal.
	 */
	private static final long serialVersionUID = -2483114542263271239L;
	private Button button1, button2, button3, button4;
	private MainFrame intance;
	public MainFrame() {
		initComponents();
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
				System.exit(0);
			}
		});
	}
	@Override
	public void initComponents() {
		setLayout(null);
		setSize(390,200);
		setResizable(false);
		
		intance = this;
		button1 = new Button("Componentes");
		button1.setBounds(80,50,100,32);
		add(button1);
		

		button2 = new Button("Test");
		button2.setBounds(250,50,100,32);
		add(button2);
		
		button1.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				ComponentsFrame cf = new ComponentsFrame(intance);
				cf.showForm();
			}
		});
		
		button2.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				TestFrame cf = new TestFrame(intance);
				cf.showForm();
			}
		});
	}
	@Override
	public void showForm() {
		setVisible(true);
		setLocationRelativeTo(null);
		toFront();
	}
	@Override
	public void clean() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void showForm(boolean maximize) {

	}
}
