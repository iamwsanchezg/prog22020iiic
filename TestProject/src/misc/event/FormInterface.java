package misc.event;

public interface FormInterface {
	void initComponents();
    void clean();
    void showForm();
    void showForm(boolean maximize);
}
