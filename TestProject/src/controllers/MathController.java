package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import models.MathOperation;
import views.MathFrame;

public class MathController implements ActionListener,KeyListener {

	private MathFrame frame;
	private MathOperation operation;

	public MathController(MathFrame mathframe) {
		frame = mathframe;
		operation = new MathOperation();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
			case "calculate":
				operation = frame.getData();
				frame.showResult(operation.getResultAsString());
				break;
			case "clean":
				frame.clean();
				break;
		}

	}
	@Override
	public void keyTyped(KeyEvent e) {
        char c= e.getKeyChar();
        if(!(Character.isDigit(c)||c==KeyEvent.VK_BACK_SPACE||c==KeyEvent.VK_ENTER ||c=='.')){
                e.consume();
        }		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
