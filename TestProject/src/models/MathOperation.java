package models;
import misc.util.Math;
/*
 * Licencia GPL.
 * Desarrollado por: William S�nchez
 * El software se proporciona "TAL CUAL", sin garant�a de ning�n tipo,
 * expresa o impl�cita, incluyendo pero no limitado a las garant�as de
 * comerciabilidad y adecuaci�n para un particular son rechazadas.
 * En ning�n caso el autor ser� responsable por cualquier reclamo,
 * da�o u otra responsabilidad, ya sea en una acci�n de contrato,
 * agravio o cualquier otro motivo, de o en relaci�n con el software
 * o el uso u otros tratos en el software.
*/

public class MathOperation {
	private Double term1;
	private Double term2;
	private Double result;
	private String operation;
	
	public MathOperation() {
		super();
		term1 = 0d;
		term2 = 0d;
	}
	public MathOperation(Double term1, Double term2) {
		super();
		this.term1 = term1;
		this.term2 = term2;
	}
	public Double getTerm1() {
		return term1;
	}
	public void setTerm1(Double term1) {
		this.term1 = term1;
	}
	public Double getTerm2() {
		return term2;
	}
	public void setTerm2(Double term2) {
		this.term2 = term2;
	}
	
    public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	private void resolve(){        
		switch(getOperation()) {
		case "add":
			result = term1+term2;
			break;
		case "substract":
			result = term1-term2;
			break;
		case "multiply":
			result = term1*term2;
			break;			
		case "divide":
			result = term1/term2;
			break;
		default:
			result = 0d;
		}		
    }
	public Double getResult() {
		resolve();
		return result;
	}
	public void setResult(Double result) {
		this.result = result;
	}
	public String getResultAsString() {
		resolve();
		return Math.getNumberToString(result);
	}
}
