package views.components.panels;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Panel;
import java.awt.TexturePaint;
import java.awt.Toolkit;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class ImageViewer extends Panel{
	/**
	 * @author William S�nchez 
	 */
	private static final long serialVersionUID = -5836574216810520452L;
	private BufferedImage bufferedImage;
	private Graphics graphics;
	private Toolkit toolkit;
	private MediaTracker tracker;
	private int width;
	private int height;
	public ImageViewer() {
        toolkit = Toolkit.getDefaultToolkit();
        tracker = new MediaTracker(this);       
	}
	public void LoadImage(String fileName, boolean asResource) {
        Image tempImage;
        if(asResource) {
        	tempImage = toolkit.getImage(getClass().getResource(fileName));
        }
        else {
        	tempImage = toolkit.getImage(fileName);
        }
        
        Image image = tempImage.getScaledInstance(getWidth(), getHeight(), Image.SCALE_DEFAULT);
        tracker.addImage(image, 0);
         try {
           // load all the image for later use
             tracker.waitForAll();
         } catch (InterruptedException ex) {
         }
        
         width = image.getWidth(this);
         height = image.getHeight(this);
        
         bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
         graphics = bufferedImage.getGraphics();
         graphics.drawImage(image, 0, 0, this);
         
         if(!asResource) {
        	 repaint();
         }

	}
	@Override
    public void paint(Graphics g) {
        setBackground(Color.WHITE);        
        Graphics2D g2 = (Graphics2D)g;        
        TexturePaint paint = new TexturePaint(bufferedImage, new Rectangle2D.Double(0,0,width,height));
        g2.setPaint(paint);
        g2.fill(new Rectangle2D.Double(0,0,width,height));        
    }
}
