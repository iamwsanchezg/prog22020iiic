package views;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class FileBrowser extends WFrame implements FilenameFilter, ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4833637100662289555L;
	FileDialog fileDialog;
	TextField directoryTextField;
	TextField fileNameTextField;
	TextField filterTextField;
	Button loadButton;
	Button saveButton;
	
	FileBrowser(Frame parentFrame) {
		super(parentFrame);	
		initComponents();
	}
	@Override
	public void initComponents() {
		
		loadButton = new Button("Abrir");
		loadButton.setActionCommand("load");
		loadButton.addActionListener(this);
		saveButton = new Button("Guardar");
		saveButton.setActionCommand("save");
		saveButton.addActionListener(this);		
		
		add("West", loadButton);
		add("East", saveButton);

		directoryTextField = new TextField();
		fileNameTextField = new TextField();
		filterTextField = new TextField();
		
		Panel p = new Panel();
		p.setLayout(new GridBagLayout());
		addRow(p, new Label("Directorio:", Label.RIGHT), directoryTextField);
		addRow(p, new Label("Archivo:", Label.RIGHT), fileNameTextField);
		addRow(p, new Label("Filtro:", Label.RIGHT), filterTextField);

		add("South", p);
		pack();
	}
	
	// Adds a row in a gridbag layout where the c2 is stretchy
	// and c1 is not.
	private void addRow(Container cont, Component c1, Component c2) {
		GridBagLayout gbl = (GridBagLayout)cont.getLayout();
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		cont.add(c1);
		gbl.setConstraints(c1, c);

		c.gridwidth = GridBagConstraints.REMAINDER;
		c.weightx = 1.0;
		cont.add(c2);
		gbl.setConstraints(c2, c);
	}
	@Override
	public boolean accept(File dir, String name) {
		System.out.println("File "+dir+" String "+name);
		if (fileDialog.getMode() == FileDialog.LOAD) {
			return name.lastIndexOf(filterTextField.getText()) > 0;
		}
		return true;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		boolean load = "load".equals(e.getActionCommand());

		if (load || "save".equals(e.getActionCommand())) {
			fileDialog = new FileDialog(this, null,
					load ? FileDialog.LOAD : FileDialog.SAVE);
			fileDialog.setDirectory(directoryTextField.getText());
			fileDialog.setFile(fileNameTextField.getText());
			fileDialog.setFilenameFilter(this);
			fileDialog.setVisible(true);
			directoryTextField.setText(fileDialog.getDirectory());
			fileNameTextField.setText(fileDialog.getFile());

			// Filter must be the same
			if (fileDialog.getFilenameFilter() != this) {
				throw new RuntimeException("Internal error");
			}
		}
	
	}



}
