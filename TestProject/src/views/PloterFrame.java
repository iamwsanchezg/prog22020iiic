package views;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;

public class PloterFrame extends WFrame {

	/**
	 * @author William S�nchez 
	 */
	private static final long serialVersionUID = -1349842383970634725L;
	
	public PloterFrame(Frame parentFrame) {
		super(parentFrame);
		initComponents();
	}
	@Override
	public void initComponents() {
		setSize(400,200);
	}
	@Override
	public void paint(Graphics g) {
		g.setColor(Color.green);
		g.drawLine(25,50,275,50);
		
		g.setColor(Color.red);
		g.drawRect(25,75,50,75);
		
		g.setColor(Color.CYAN);
		g.fill3DRect(100,75,50,50,true);	
		
		g.setColor(Color.ORANGE);
		g.drawOval(175,75,50,50);

		g.setColor(Color.BLACK);
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		String[] fonts = ge.getAvailableFontFamilyNames();
		g.setFont(new Font(fonts[0],Font.PLAIN,16));
		g.drawString("Cuadrado",100,150);
		
		g.setFont(new Font(fonts[0],Font.BOLD,18));
		g.drawString("C�rculo",200,150);

		g.setColor(Color.blue);
		int x[] = {250, 275, 300, 325, 350, 375, 400};
		int y[] = {100, 75, 100, 75, 100, 75, 100};
		g.drawPolygon(x,y,6);
	}

}
