package views;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;

import controllers.MathController;
import models.MathOperation;

public class MathFrame extends WFrame{
	/**
	 * @author William S�nchez 
	 */
	private static final long serialVersionUID = -6042695745770681524L;
	private Label label1, label2, label3;
	private TextField textField1, textField2,textField3;	
	private CheckboxGroup group1;
	private Checkbox checkbox1, checkbox2, checkbox3,checkbox4;	
	private Button button1, button2;
	private MathController mc;
	public MathFrame(Frame parentFrame) {
		super(parentFrame);
		initComponents();
	}	
	@Override
	public void initComponents() {
		setLayout(null);
		mc = new MathController(this);
		
		label1 = new Label("N�mero 1");
		label1.setBounds(20, 50, 100, 32);
		add(label1);		
		
		textField1 = new TextField("");
		textField1.setBounds(122,50,250,32);
		textField1.addKeyListener(mc);
		add(textField1);
		
		label2 = new Label("N�mero 2");
		label2.setBounds(20, 84, 100, 32);
		add(label2);		
		
		textField2 = new TextField("");
		textField2.setBounds(122,84,250,32);
		textField2.addKeyListener(mc);
		add(textField2);
		
		label3 = new Label("Resultado");
		label3.setBounds(20, 118, 100, 32);
		add(label3);
		
		textField3 = new TextField("");
		textField3.setBounds(122,118,250,32);
		textField3.setEnabled(false);
		add(textField3);
		
		group1 = new CheckboxGroup();
		checkbox1 = new Checkbox("Suma", group1, true);
		checkbox1.setBounds(380,50,100,24);
		add(checkbox1);
		
		checkbox2 = new Checkbox("Resta", group1, false);
		checkbox2.setBounds(380,75,100,24);
		add(checkbox2);
		
		checkbox3 = new Checkbox("Multiplicaci�n", group1, false);
		checkbox3.setBounds(380,100,100,24);
		add(checkbox3);
		
		checkbox4 = new Checkbox("Divisi�n", group1, false);
		checkbox4.setBounds(380,125,100,24);
		add(checkbox4);
		
		button1 = new Button("Borrar");
		button1.setBounds(20,175,100,32);
		button1.setActionCommand("clean");
		button1.addActionListener(mc);
		add(button1);
		
		button2 = new Button("Calcular");
		button2.setBounds(370,175,100,32);
		button2.setActionCommand("calculate");
		button2.addActionListener(mc);
		add(button2);
		
		setTitle("Operaciones matem�ticas");
		setSize(500,250);
	}
	public MathOperation getData() {
		MathOperation mo = new MathOperation();
		mo.setTerm1(Double.parseDouble(textField1.getText()));
		mo.setTerm2(Double.parseDouble(textField2.getText()));
		Checkbox ck = group1.getSelectedCheckbox();
		switch(ck.getLabel()) {
			case "Suma":
				mo.setOperation("add");
				break;
			case "Resta":
				mo.setOperation("substract");
				break;
			case "Multiplicaci�n":
				mo.setOperation("multiply");
				break;
			case "Divisi�n":
				mo.setOperation("divide");
				break;
			default:
				mo.setOperation("none");
				break;
		}
		return mo;
	}
	public void showResult(String result) {
		textField3.setText(result);
	}
	@Override
	public void clean() {
		textField1.setText("");
		textField2.setText("");
		textField3.setText("");
		textField1.requestFocus();
	}
}
