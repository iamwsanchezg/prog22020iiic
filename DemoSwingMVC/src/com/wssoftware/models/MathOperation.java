/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package com.wssoftware.models;
import com.wssoftware.misc.Math;
/**
 *
 * @author William Sanchez
 */
public class MathOperation {
    private Double term1;
    private Double term2;
    private Double result;
    private String operation;

    public MathOperation() {
    }

    public MathOperation(Double term1, Double term2, String operation) {
        this.term1 = term1;
        this.term2 = term2;
        this.operation = operation;
    }

    public Double getTerm1() {
        return term1;
    }

    public void setTerm1(Double term1) {
        this.term1 = term1;
    }

    public Double getTerm2() {
        return term2;
    }

    public void setTerm2(Double term2) {
        this.term2 = term2;
    }

    public Double getResult() {
        resolve();
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
    	private void resolve(){        
            switch(getOperation()) {
            case "add":
                    result = term1+term2;
                    break;
            case "substract":
                    result = term1-term2;
                    break;
            case "multiply":
                    result = term1*term2;
                    break;			
            case "divide":
                    result = term1/term2;
                    break;
            default:
                    result = 0d;
            }		
    }
	public String getResultAsString() {
		resolve();
		return Math.getNumberToString(result);
	}        
        
}
