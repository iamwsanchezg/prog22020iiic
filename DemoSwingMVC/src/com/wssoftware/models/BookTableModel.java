/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package com.wssoftware.models;

import java.lang.reflect.Field;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author William Sanchez
 */
public class BookTableModel extends AbstractTableModel{
    private Object columnNames[];
    private Object rowData[][];
    
    Class[] types;
    boolean[] canEdit;

    private void setup(){
        Field fields[] = Book.class.getDeclaredFields();
        columnNames = new String[fields.length];
        types  = new Class [fields.length];
        canEdit = new boolean [fields.length];
        int c=0;
        for (Field field:fields){
            columnNames[c]=field.getName();
            types[c] = field.getType();
            canEdit[c] = true;
            c++;
        }
    }
    public BookTableModel(){
        setup();
    }
    public BookTableModel(Book[] bookList){
        setup();
        rowData = new Object[bookList.length][columnNames.length];
        int c=0;
        for(Book book:bookList){
            rowData[c]=new Object[]{
                                    book.getTitle(),
                                    book.getCategory(),
                                    book.getAuthor(),
                                    book.getPublisher(),
                                    book.getDescription()
                                };
            c++;
        }
    }
    public void setDataModel(Object[][] data){
         rowData = data;
    }
    public TableModel getModel(){
      TableModel model = new DefaultTableModel(
            rowData,
            columnNames
        ){
            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        };
        return model;
    }
    public Book[] getData(){
        Book[] bookList;
        if(rowData!=null){
            bookList = new Book[rowData.length];
            for(int i=0;i<rowData.length;i++){
                Book b = new Book();
                b.setTitle(String.valueOf(rowData[i][0]));
                b.setCategory(String.valueOf(rowData[i][1]));
                b.setAuthor(String.valueOf(rowData[i][2]));
                b.setDescription(String.valueOf(rowData[i][3]));
                bookList[i] = b;                
            }
        }
        else{
            bookList = new Book[0];
        }
        return bookList;
    }
    @Override
    public int getRowCount() {
        if(rowData==null) {
            return 0;   
        }
        else{            
            return rowData.length;
        }
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return rowData[rowIndex][columnIndex];
    }
    public static Object[] getDefaultRowData(){
        Book b = new Book();
        return new Object[]{
                    b.getTitle(),
                    b.getCategory(),
                    b.getAuthor(),
                    b.getPublisher(),
                    b.getDescription()
                };
    }
}
