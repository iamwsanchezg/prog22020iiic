/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package com.wssoftware.controllers;

import com.wssoftware.views.DemoDateFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;

/**
 *
 * @author William Sanchez
 */
public class DemoDateController implements ActionListener{
    JFileChooser d;
    DemoDateFrame demodateframe;

    public DemoDateController(DemoDateFrame demodateframe) {
        this.demodateframe = demodateframe;
        d = new JFileChooser();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "okButton":
                saveDemoDate();
                break;
        }
    }
    public void saveDemoDate(){
        d.showSaveDialog(demodateframe);
        File file = d.getSelectedFile();
        if(file!=null){
            try{
                ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
                w.writeObject(demodateframe.getData());
                w.flush();
            }catch(IOException ex){

            }
        }
    }
}
