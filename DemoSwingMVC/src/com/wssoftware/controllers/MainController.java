/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package com.wssoftware.controllers;

import com.wssoftware.views.BookFrame;
import com.wssoftware.views.BookManagerFrame;
import com.wssoftware.views.DemoComboBoxFrame;
import com.wssoftware.views.DemoDateFrame;
import com.wssoftware.views.MainFrame;
import com.wssoftware.views.MathOperationFrame;
import com.wssoftware.views.Notepad;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;

/**
 *
 * @author William Sanchez
 */
public class MainController implements ActionListener{
    MainFrame frame;
    JFileChooser d;
    static int nn=0;
    public MainController(MainFrame frame) {
        this.frame = frame;
        d = new JFileChooser();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "exit":
                System.exit(0);
                break;
            case "create":
                invokeCreate();
                break;
            case "manage":
                invokeManage();
                break;
            case "notepad":
                invokeNotepad();
                break;
            case "saveFromMain":
                saveNotepad();
                break;
            case "mathOperation":
                invokeMathOperation();
                break;
            case "demoDate":
                invokeDemoDate();
                break;
            case "demoCombobox":
                invokeDemoComboBox();
                break;
            default:
                break;
        }
    }
    public void invokeDemoComboBox(){
        DemoComboBoxFrame dcbf = new DemoComboBoxFrame();
        frame.showChild(dcbf, false);
    }
    public void invokeDemoDate(){
        DemoDateFrame ddf = new DemoDateFrame();
        frame.showChild(ddf, false);
    }
    public void invokeCreate(){
        BookFrame bf = new BookFrame();
        frame.showChild(bf, false);
    }
    public void invokeManage(){
        BookManagerFrame bm = new BookManagerFrame();
        frame.showChild(bm, true);
    }
    public void invokeNotepad(){
        nn++;
        Notepad notepad = new Notepad();
        notepad.setTitle("Bloc de notas " + String.valueOf(nn));
        frame.showChild(notepad, true);
    }
    public void saveNotepad(){
        d.showSaveDialog(frame);
        File file = d.getSelectedFile();
        String text = "";
        JDesktopPane desktop =  frame.getDesktopPane();
        for(JInternalFrame frame: desktop.getAllFrames()){
            if(frame.isSelected()){
                if(Notepad.class.getName()==frame.getClass().getName()){
                   text = ((Notepad)frame).getFullText();
                }
            }
        }
         try{
             
             Writer w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
            w.write(text);
            w.flush();
        }catch(IOException ex){
            
        }       
    }
    public void invokeMathOperation(){
        MathOperationFrame mof = new MathOperationFrame();
        frame.showChild(mof, false);
    }
}
