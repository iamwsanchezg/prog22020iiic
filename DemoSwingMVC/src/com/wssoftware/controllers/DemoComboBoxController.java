/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package com.wssoftware.controllers;

import com.wssoftware.views.DemoComboBoxFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author William Sanchez
 */
public class DemoComboBoxController implements ActionListener{
    DemoComboBoxFrame dcbf;

    public DemoComboBoxController(DemoComboBoxFrame dcbf) {
        this.dcbf = dcbf;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "listNameComboBoxChanged" -> {
                JComboBox cbo = (JComboBox)e.getSource();
                setupData(cbo);
            }
            case "itemListComboBoxChanged" -> {
            }
            case "okButton" -> showSelectedData();
        }
    }    
   public void setupData(JComboBox cbo){
        String title = cbo.getSelectedItem().toString();
        int index = cbo.getSelectedIndex();
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        switch(index){
            case 0 -> {
                model.addElement("Gato");
                model.addElement("Perro");
                model.addElement("Gallina");
                model.addElement("Vaca");
            }
            case 1 -> {
                model.addElement("Mariposa");
                model.addElement("Lombris");
                model.addElement("Babosa");
                model.addElement("Caracol");
            }
            case 2 -> {
                model.addElement("Mango");
                model.addElement("Jocote");
                model.addElement("Mandarina");
                model.addElement("Zapote");
            }
            case 3 -> {
                model.addElement("Zanahoria");
                model.addElement("Remolacha");
                model.addElement("Lechuga");
                model.addElement("Repollo");
            }
        }
        dcbf.setupData(title, model);
   }
   public void showSelectedData(){
       JOptionPane.showMessageDialog(dcbf, dcbf.getSelectedData());
   }
}
