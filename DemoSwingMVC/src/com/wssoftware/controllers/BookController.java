/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package com.wssoftware.controllers;

import com.wssoftware.models.Book;
import com.wssoftware.models.BookTableModel;
import com.wssoftware.views.BookFrame;
import com.wssoftware.views.BookManagerFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;

/**
 *
 * @author William Sanchez
 */
public class BookController implements ActionListener{
    private BookFrame bframe;
    private BookManagerFrame mframe;
    private Book book;
    JFileChooser d;
    public BookController(BookFrame bf){
        bframe = bf;
        d = new JFileChooser();
    }

    public BookController(BookManagerFrame mframe) {
        this.mframe = mframe;
        d = new JFileChooser();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "clean":
                bframe.clean();
                break;
            case "save":
                d.showSaveDialog(bframe);
                File file = d.getSelectedFile();
                book = bframe.getBookData();
                save(file);
                break;
            case "load":
                d.showOpenDialog(bframe);
                book = open(d.getSelectedFile());
                bframe.setBookData(book);
                break;
            case "listDirectoryBooks":
                showBooksOnTable();
                break;
            case "newRow":
                mframe.addRow();
                break;
            case "removeRow":
                mframe.removeRow();
                break;
            case "saveTableContent":
                saveTableContent();
                break;
        }
    }
    
    public void save(File file){
        try{
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(book);
            w.flush();
        }catch(IOException ex){
            
        }
    }
    public Book open(File file) {
        try{
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Book)ois.readObject();
        }catch(IOException|ClassNotFoundException ex){
            
        }
        return null;
    }
    private Book [] readBookList(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
        Book [] books;
        try(
            FileInputStream in = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(in)
            )
            {
                books = (Book []) ois.readObject();
            }
        return books;
    }
    public void showBooksOnTable(){
        d.showOpenDialog(mframe);
        File bl = d.getSelectedFile();
        if(bl!=null){
            mframe.setBookListFile(bl.getPath());
            try{
                mframe.setBookTableModel(new BookTableModel(readBookList(bl)).getModel());
            }
            catch(IOException|ClassNotFoundException ex){
                System.out.println(ex.getMessage());
            }
        }
    }
    public void saveTableContent(){
        d.showSaveDialog(mframe);
        File bl = d.getSelectedFile();
        if(bl!=null){
            Book [] books = mframe.getTableData();
            try{
                ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(bl));
                w.writeObject(books);
                w.flush();
            }catch(IOException ex){

            }
        }
    }
    
}
